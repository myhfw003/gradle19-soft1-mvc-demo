﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SayDemo.ViewModel
{
    public class SayViewModel
    {
        public string Username { get; set; }
        public IEnumerable<MessageViewModel> Messages { get; set; }
    }
}