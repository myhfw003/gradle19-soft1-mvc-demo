﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SayDemo.Models
{
    public abstract class BaseModel
    {
        public BaseModel()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
            Version = 0;
        }

        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int Version { get; set; }
        public string Remarks { get; set; }
    }
}