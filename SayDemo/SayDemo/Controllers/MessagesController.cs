﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SayDemo.Data;
using SayDemo.Models;
using SayDemo.ViewModel;

namespace SayDemo.Controllers
{
    public class MessagesController : Controller
    {
        private SayDemoDb db = new SayDemoDb();

        // GET: Messages
        public ActionResult Index()
        {
            var id = 1;
            var currentUser = db.Users.Where(x=>x.Id==id).SingleOrDefault();

            // 获取所有的用户
            var userList = db.Users.ToList();
            // 获取所有的说说
            var msgList = db.Messages.ToList();
            // 所有的评论
            var comList = db.Comments.ToList();
            
            // 定义一个准备用来放所有的首页消息模型的集合
            var msgViewModelList = new List<MessageViewModel>();

            // 遍历所有的消息
            foreach(var msg in msgList)
            {
                // 定义一个集合，用来存放当前消息下的所有的评论
                var tmpComList = new List<CommentViewModel>();
                // 根据当前说说id，查找所有的评论
                var currentMsgComments = comList.Where(x => x.MsgId == msg.Id).ToList();
               

                // 遍历当前说说的所有的评论
                foreach(var c in currentMsgComments)
                {
                    tmpComList.Add(new CommentViewModel
                    {
                        FromUserName = userList.SingleOrDefault(x => x.Id == c.FromUserId).Username,// 根据当前的评论用户id，从用户中查找对应用户，并且获得用户的名称
                        ToUserName = userList.SingleOrDefault(x => x.Id == msgList.SingleOrDefault(t => t.Id == c.MsgId).FromUserId).Username,// 
                        Comment = c.Content
                    });
                }

                // 将当前说说转换为首页说说视图模型
                msgViewModelList.Add(new MessageViewModel
                {
                    Id = msg.Id,
                    FromUserId = msg.FromUserId,
                    Content = msg.Content,
                    Comments = tmpComList
                });
            }


            var data = new SayViewModel
            {
                Username = currentUser.Username,
                Messages = msgViewModelList
            };


            return View(data);
        }

        // GET: Messages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = db.Messages.Find(id);
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // GET: Messages/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Messages/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FromUserId,Content,CreatedAt,UpdatedAt,Version,Remarks")] Messages messages)
        {
            if (ModelState.IsValid)
            {
                db.Messages.Add(messages);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(messages);
        }

        // GET: Messages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = db.Messages.Find(id);
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // POST: Messages/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FromUserId,Content,CreatedAt,UpdatedAt,Version,Remarks")] Messages messages)
        {
            if (ModelState.IsValid)
            {
                db.Entry(messages).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(messages);
        }

        // GET: Messages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = db.Messages.Find(id);
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // POST: Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Messages messages = db.Messages.Find(id);
            db.Messages.Remove(messages);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
