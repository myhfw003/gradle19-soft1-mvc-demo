﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SayDemo.Data;
using SayDemo.Models;
using SayDemo.ParamModel;

namespace SayDemo.Controllers
{
    public class UsersController : Controller
    {
        private SayDemoDb db = new SayDemoDb();

        // GET: Users
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Username,Password,CreatedAt,UpdatedAt,Version,Remarks")] Users users)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(users);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(users);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: Users/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Username,Password,CreatedAt,UpdatedAt,Version,Remarks")] Users users)
        {
            if (ModelState.IsValid)
            {
                db.Entry(users).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(users);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Users users = db.Users.Find(id);
            db.Users.Remove(users);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Register()
        {


            return View();
        }

        public ActionResult Login(LoginModel login)
        {                     

            return View();
        }

        [HttpPost]
        public string registerDone(RegisterModel registerModel)
        {
            var username = registerModel.Username.Trim();
            var password = registerModel.Password.Trim();
            var confirmPassword = registerModel.ConfirmPassword.Trim();

            dynamic result;

            // 判断用户名、密码是否为空，不为空则继续；否则直接返回信息
            if(username.Length>0 && password.Trim().Length>0 && password.Equals(confirmPassword))
            {
                var user = db.Users.Where(x => x.Username.Equals(username)).FirstOrDefault();

                // 如果用户不为空，则表示该用户名已经注册
                if (user != null)
                {
                    result = new
                    {
                        code = 1000,
                        msg = "当前用户名已经存在"
                    };
                }
                else
                {
                    db.Users.Add(new Users
                    {
                        Username = username,
                        Password = password
                    });

                    db.SaveChanges();


                    result = new
                    {
                        code = 200,
                        msg = "注册成功"
                    };
                }


            }
            else
            {
                result = new 
                { 
                    code=1000,
                    msg="用户名密码不能为空，并且两次密码应该一致，请核对后重试=_="
                };
            }


            return JsonConvert.SerializeObject(result);
        }

        public string LoginDone (LoginModel login)
        {
            dynamic result;

            if (login.Username.Trim().Length > 0 && login.Password.Trim().Length > 0)
            {
                var user = db.Users.Where(x => x.Username.Equals(login.Username.Trim()) && x.Password.Equals(login.Password.Trim())).FirstOrDefault();

                if (user != null)
                {
                    result = new
                    {
                        code = 200,
                        msg = "登录成功"
                    };
                }
                else
                {
                    result = new
                    {
                        code = 1000,
                        msg = "用户名或密码不正确，请核对后重试"
                    };
                }
            }
            else
            {
                result = new
                {
                    code = 1000,
                    msg = "用户名密码不能为空，请核对后重试=_="
                };
            }

            return JsonConvert.SerializeObject(result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
